#!/bin/sh
#SBATCH --job-name=test-palabos
#SBATCH --cpus-per-task=1
#SBATCH --tasks=16
#SBATCH --partition=debug
#SBATCH --time=15:00
#SBATCH --reservation=capello_96

module load foss/2018b

# resolution http://wiki.palabos.org/plb_wiki:benchmarks
N=400

srun palabos-v2.0r0/examples/benchmarks/cavity3d/cavity3d $N
