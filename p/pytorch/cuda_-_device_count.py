#!/usr/bin/env python
#
# <https://pytorch.org/docs/stable/cuda.html>


### ATTENTION, print behavior changed between Python 2.x and 3.x!
from __future__ import print_function

import torch
device_count = torch.cuda.device_count()
print("torch.cuda.device_count:", device_count)
