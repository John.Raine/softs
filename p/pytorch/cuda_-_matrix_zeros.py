#!/usr/bin/env python
#
# Create a 10x10 matrix filled with zero via the CUDA device
#   <https://hpc-community.unige.ch/t/gpu010-cuda-singularity-cuda-runtime-error/306>


import torch
cuda = torch.device('cuda')
a = torch.zeros(10, device=cuda)
print(a)
