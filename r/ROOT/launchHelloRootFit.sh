#!/bin/sh

#SBATCH --partition=debug-EL7
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --job-name=HelloRootFit

module load foss/2018b LibTIFF/4.0.9 ROOT/6.14.06-Python-2.7.15

srun python helloRootFit.py
