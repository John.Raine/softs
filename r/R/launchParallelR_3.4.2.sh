#!/bin/bash
#SBATCH --job-name=testR
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=20
#SBATCH --time=0:15:0
#SBATCH --partition=shared


module load foss/2016b R/3.4.2

INFILE=helloParallel.R
OUTFILE=helloParallel-${SLURM_JOBID}.Rout

srun R CMD BATCH $INFILE $OUTFILE

