#!/bin/sh
#SBATCH --tasks 1
#SBATCH --cpus-per-task 12
#SBATCH --partition shared-gpu
#SBATCH --gres gpu:titan:2
#SBATCH --time 15:00

module load intel/2017a Blender/2.79-Python-3.6.1
module load CUDA/8.0.61


# see the rendering engines available
#srun blender --background --engine help


#BLENDER_FILE=bmw27/bmw27_cpu.blend
BLENDER_FILE=bmw27/bmw27_gpu.blend
PYTHON_SCRIPT=gpurender.py

srun blender --background \
 ${BLENDER_FILE} \
 --threads 12 \
 #--enable-autoexec \
 --python ${PYTHON_SCRIPT} \
 --render-output //out \
 --render-format PNG \
 --render-frame 5
