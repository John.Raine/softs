#!/bin/sh 
#SBATCH --cpus-per-task=1
#SBATCH --job-name=testTensorFlow
#SBATCH --ntasks=1
#SBATCH --time=05:00
#SBATCH --output=slurm-%J.out
#SBATCH --gres=gpu:1
#SBATCH --constraint="V5|V6"
#SBATCH --partition=shared-gpu-EL7

module load foss/2016a tensorflow/1.3.0-Python-2.7.11 cuDNN/5.1-CUDA-8.0.44

srun python helloworld.py
