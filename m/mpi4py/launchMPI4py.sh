#!/bin/bash
#SBATCH --job-name=testMPI4py
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=2
#SBATCH --time=00:02:00
#SBATCH --partition=debug-EL7

module load GCC/7.3.0-2.30  OpenMPI/3.1.1 Python/3.6.6

srun python point-to-point.py
