#!/bin/bash
#SBATCH --partition=debug
#SBATCH --ntasks=1

# launch a matlab binary (compiled .m)

module load foss/2016a matlab/2016b

srun ./hello $NAME $AGE
