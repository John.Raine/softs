% create a temporary directory for parcluster
% this directory is removed automatically a the end of execution
t=tempname();
mkdir(t);
disp(['temporary space for parcluster: ', t]);

% some cleanup
c=parcluster('local');
c.JobStorageLocation=t;
delete(c.Jobs);

% get the number of cores allocated by slurm
n=str2num(getenv('SLURM_CPUS_PER_TASK'));

% create a parpool
parpool(c, n);

parfor i=1:n
  for j=1:100000000
    a=sin(j);
    a=cos(j);
    if ~rem(j,1000000)
      x = ['worker: ', num2str(i), ' iteration: ', num2str(j), ' value: ', num2str(a)];
       disp(x);
    end
  end
end
