#!/bin/bash

#SBATCH --licenses=matlab@matlablm.unige.ch

BASE_MFILE_NAME=test
MATLAB_MFILE=${BASE_MFILE_NAME}.m 

unset DISPLAY

module load matlab

job_array_index=${SLURM_ARRAY_TASK_ID}

echo "Starting at $(date)"
echo "Running ${MATLAB_MFILE} on $(hostname)"

srun matlab -nodesktop -nosplash -nodisplay -r "${BASE_MFILE_NAME}($job_array_index)"

echo "Finished at $(date)"
