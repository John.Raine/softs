""" A first plot - get some numbers, plot them against
a uniform X axis starting at 1 using all the defaults."""

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

plt.ioff()
plt.plot([1,23,2,4])
plt.ylabel('some numbers')

plt.savefig('example.png')
