#!/bin/sh 
#SBATCH --cpus-per-task=1
#SBATCH --job-name=test-Meep
#SBATCH --ntasks=16
#SBATCH --mem=20G
#SBATCH --output=slurm-%J.out

# the gcc version doesn't work and crash at runtime.
module load intel/2016a Meep/1.3

srun meep-mpi no_struc=true hynobius.ctl 
