#!/bin/sh
#SBATCH --gres=gpu:1
#SBATCH --constraint="COMPUTE_CAPABILITY_6_0|COMPUTE_CAPABILITY_6_1"
#SBATCH --partition=shared-gpu

module load foss/2016a Keras/1.2.2-Python-2.7.11 cuDNN/5.1-CUDA-8.0.44 protobuf-python/3.0.2-Python-2.7.11


srun python testKeras.py

